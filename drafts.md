    $ ./excom examplehost.com

    ExCom on examplehost.com | SUM | List | Paniclog | Message | Help |

    --------------------------------------------------------------------
    | Count  Volume  Oldest  Newest  Domain
    |-------------------------------------------------------------------|
    |    1     963     12h     12h  domain.one
    |    9   267KB      4d     11h  domain.two
    |   94  3349KB      4d      0m  domain.three
    |
    | ------------------------------------------------------------------|
    |  105  3685KB      4d      0m  TOTAL                               |
     --------------------------------------------------------------------



    ExCom on examplehost.com    <s> Sum
                                <i> List
                                <p> Paniclog / Mainlog
                                    Message
                                <?> Help

     -------------------------------------------------------------------
    | Count  Volume  Oldest  Newest  Domain                             |
    |-------------------------------------------------------------------|
    |    1     963     12h     12h  domain.one
    |    9   267KB      4d     11h  domain.two
    |   94  3349KB      4d      0m  domain.three
    |
    |-------------------------------------------------------------------|
    |  105  3685KB      4d      0m  TOTAL                               |
     --------------------------------------------------------------------



    ExCom                       <s> Sum                <f> Filter by sender
    on examplehost.com          <i> List               <r> Filter by recipient
                                <p> Paniclog / Mainlog <d> Delete message
                                    Message            
                                <?> Help

    ╔═════════════════════════ Message list (mailq) ══════════════════════════════╗
    ║4d  111K 1oA11P-0001FU-1l <bounces+asdf@domain.com>          user@domain.com ║
    ║4d   46K 1o111L-0002yN-1Q <alerts+asdf@domain.com>          user@domain.com  ║


    ExCom                       <q> Quit                <d> Delete message
    on examplehost.com          <s> SUM                 <f> Filter by sender
                                <i> List                <r> Filter by recipient
    Refreshed 11:35             <p> Paniclog / Mainlog
                                <?> Help



    <q> Quit                 <d> Delete message        📧🎩 ExCom                       
    <s> SUM                  <f> Filter by sender      on examplehost.com          
    <i> List                 <r> Filter by recipient
    <p> Paniclog / Mainlog                             Refreshed 11:35             
    <?> Help

┌───────────────────────────────────────┬────────────────────────────────────────┬────────────────────────────────────────┐
│<q> Quit                               │<enter> show list filtered on domain    │ExCom on web.lxd                        │
│<s> show Sum                           │<d>     Delete selected messages        │ | SUM | List | Log (2) | Message | Help│
│<l> show List of messages              │                                        │ |                                      │
│<p> show (Panic) log                   │                                        │                                        │
│<t> open terminal on target            │                                        │                                        │
│                                       │                                        │                                        │
│                                       │                                        │                                        │
│                                       │                                        │                                        │
│                                       │                                        │                                        │
│                                       │                                        │                                        │
│                                       │                                        │                                        │
├───────────────────────────────────────┴────────────────────────────────────────┴────────────────────────────────────────┤
│┌─────┬──────┬──────┬──────┬───────┐                                                                                     │
││Count│Volume│Oldest│Newest│Domain │                                                                                     │
│├─────┼──────┼──────┼──────┼───────┤                                                                                     │
││     │      │      │      │       │                                                                                     │
│├─────┼──────┼──────┼──────┼───────┤                                                                                     │
││25   │9850  │26h   │26h   │auto.no│                                                                                     │
│├─────┼──────┼──────┼──────┼───────┤                                                                                     │
││1    │494   │12m   │12m   │web.lxd│                                                                                     │
│├─────┼──────┼──────┼──────┼───────┤                                                                                     │

------------------------------------------------------------------------------------
| ExCom on web.lxd  | SUM | List | Log (2) | Message | Help                        |
------------------------------------------------------------------------------------
| ┌─────┬──────┬──────┬──────┬───────┐                        | <q> Quit 
| │Count│Volume│Oldest│Newest│Domain │                        | <s> show Sum
| ├─────┼──────┼──────┼──────┼───────┤                        |
| │25   │9850  │26h   │26h   │auto.no│                        |
|                                                             |
|                                                             |
|                                                             |
|                                                             |