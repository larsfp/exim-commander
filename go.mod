module excom

go 1.18

require (
	github.com/atotto/clipboard v0.1.4
	github.com/gdamore/tcell/v2 v2.5.1
	github.com/kevinburke/ssh_config v1.2.0
	github.com/mergestat/timediff v0.0.3
	github.com/mitchellh/go-homedir v1.1.0
	github.com/rivo/tview v0.0.0-20220709181631-73bf2902b59a
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/term v0.0.0-20220722155259-a9ba230a4035
)

require golang.org/x/text v0.3.7 // indirect
