# Exim-commander

A terminal UI for Exim and Postfix mail queue.

![Logo](https://gitlab.com/larsfp/exim-commander/-/raw/master/img/excom-512.png)

A quicker way to check and process mail queue on linux servers (running exim or postfix).

Saves you from fiddling with exiqgrep, exim -Mvc and piping with xargs and exim -Mrm. Or postqueue, postconf, etc.

Usage:

    $ excom <server name>

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/excom)

# Status

Beta

# Features

* Operates via SSH. Will try ssh agent (unavailabe for snaps), ssh keys, ssh password.
* Operates directly on localhost if NOT installed via snap, due to sandboxing.
* Summary of mail queue (exim and postfix).
* List of full or filtered mail queue (exim).
* Show message with header and body (exim and postfix)
* List number of lines in panic log (exim and postfix).
* Show paniclog and mainlog (exim and postfix).
* Delete one or many mails from queue (exim and postfix).
* Truncate log (exim and postfix).
* Quickly open a terminal on server (unavailabe for snaps).
* Easy installation as single binary or via snap.
* Tested on linux, may work on macos, wsl?

## Screenshots

### v0.6

Sum

<img src="img/v0.6/sum.png" alt="Sum view">

### v0.3

Sum

<img src="img/v0.3/sum.png" alt="Sum view">

List, filtered

<img src="img/v0.3/list.png" alt="List view">

Message, with header in a different color

<img src="img/v0.3/message.png" alt="Message view">

Panic log

<img src="img/v0.3/panic.png" alt="Panic log view">

## Installation

### Binary

Find deb and tarballs at https://gitlab.com/larsfp/exim-commander/-/releases

### Snap

Install:

    $ sudo snap install excom

Allow usage of ssh-keys:

    $ sudo snap connect excom:ssh-keys :ssh-keys

SSH-agent is not available from snap.

### Source

Clone repo and run ```go build```. Go 1.18 or higher.

## Development

To have a proper terminal in vscode (needed for TUIs), add this to launch.json:

    "console": "integratedTerminal",

Tests:

    $ go test -v ./internal

## TODO

### Should have

* Postfix support (partially done, very beta)
* Open message in a new window / in a pager
* Make use of sudo if not logged in as root.
* Support for encrypted ed25519 keys.

### Nice to have

* Argument to ask for which host to use. For when started via desktop shortcut.
* Open terminal command could detect if running in tmux, screen, etc and run something like: tmux new-window -c "#{pane_current_path}" ssh HOSTNAME
* Hotkey ? to view html formatting in message view
* Add more flags from exiqgrep
* Show enabled flags
* desktop file, icon in deb package.

### Polish

* Space to multi-select in sum, list
* Feedback in status line at bottom?
* Config file? For colors, commands (i.e. terminal), periodic refresh.
* Splash while connecting?

## Inspiration

* https://github.com/darkhz/bluetuith/
* https://github.com/derailed/k9s/

## Similar programs

* pfqueue - http://pfqueue.sourceforge.net/