package internal

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"golang.org/x/crypto/ssh"
)

// Generate header text, assemble ui in a grid, pass on to page
func generateHeader(content tview.Primitive) *tview.Grid {
	globalMenuColor := tcell.ColorBlue

	modes := [4]string{"Sum", "List", "Paniclog", "Message"}
	modelist := " | "
	for _, m := range modes {
		if currentMode == strings.ToLower(m) {
			if strings.ToLower(m) == "paniclog" {
				m = "Log (" + strconv.Itoa(paniclogLength) + ")"
			}
			modelist += strings.ToUpper(m) + " | "
		} else {
			if strings.ToLower(m) == "paniclog" {
				m = "Log (" + strconv.Itoa(paniclogLength) + ")"
			}
			modelist += m + " | "
		}
	}

	// Generate grid
	grid = tview.NewGrid()
	if *verbose {
		grid.SetBorders(false)
	}
	grid.SetTitle("Grid")
	grid.SetRows(1, 0)
	grid.SetColumns(0, 40)

	mgh := 0 // mingridheight
	mgw := 1 //mingridwidth

	// Col 0 row, col, rowspan, colspan, mingridheight, mingridwidth, focus
	col := 0
	row := 0

	// Header
	topString := projectName + " v" + projectVersion + " on " + fqdn + modelist
	// grid.AddItem(tview.NewTextView().
	// 	SetText(projectName+" on "+fqdn+modelist+"(narrow mode)").
	// 	SetTextColor(globalMenuColor), 0, col, 1, 2, mgh, mgw, false)
	// grid.AddItem(tview.NewTextView().
	// 	SetText(projectName+" v"+projectVersion+" on "+fqdn+modelist+"(width of >100 will show sidebar)").
	// 	SetTextColor(globalMenuColor), 0, col, 1, 1, mgh, 100, false)
	grid.AddItem(tview.NewTextView().
		SetText(topString).
		SetTextColor(globalMenuColor), 0, col, 1, 2, mgh, mgw, false)

	// Show filter input
	if currentMode == "list" && len(fromfilter) > 0 {
		row++
		grid.SetRows(1, 3, 0)
		fromfilterinput.SetLabel(" From-filter (-f): ")
		fromfilterinput.SetBorderPadding(1, 1, 1, 1)
		fromfilterinput.SetText(strings.TrimSpace(fromfilter))
		fromfilterinput.SetDoneFunc(func(key tcell.Key) {
			fromfilter = fromfilterinput.GetText()
			generateListPage(sshc, true, fromfilterinput)
			app.SetFocus(msglist)
		})
		grid.AddItem(fromfilterinput, row, col, 1, 1, mgh, mgw, false)
	}

	if currentMode == "list" && len(recipientfilter) > 0 {
		row++
		grid.SetRows(1, 3, 0)
		recipientfilterinput.SetLabel(" Recipient-filter (-r): ")
		recipientfilterinput.SetLabelColor(tcell.ColorOrange)
		recipientfilterinput.SetBorderPadding(1, 1, 1, 1)
		recipientfilterinput.SetText(strings.TrimSpace(recipientfilter))
		recipientfilterinput.SetDoneFunc(func(key tcell.Key) {
			// On no change, no reaction
			if strings.TrimSpace(recipientfilter) == strings.TrimSpace(recipientfilterinput.GetText()) {
				app.SetFocus(msglist)
				return
			}
			if key == tcell.KeyEnter {
				recipientfilter = recipientfilterinput.GetText()
				generateListPage(sshc, true, msglist)
				return
			}
		})
		grid.AddItem(recipientfilterinput, row, col, 1, 1, mgh, mgw, false)
	}

	// Main content
	row++
	padframe := tview.NewFrame(content)
	padframe.SetBorderPadding(1, 1, 1, 1)

	grid.AddItem(padframe, row, col, 1, 2, mgh, mgw, true)

	// Draw sidebar
	if showSidebar {
		sidebarText := "Hotkeys\n\n" +
			"<q>     Quit\n\n" +
			"<1>     show Sum\n" +
			"<2>     show List of messages\n" +
			"<3>     show (Panic) log\n" +
			"<t>     open terminal on target\n" +
			"<b>     show/hide sidebar\n" +
			"<y>     yank content\n"

		if currentMode == "sum" {
			sidebarText += "\n<enter> show list filtered on domain\n<d>     Delete selected messages\n"
		} else if currentMode == "list" {
			sidebarText += "\n<enter> show message\n<d>     Delete selected message\n<f>     Filter based on sender\n<r>     Filter based on recipient\n"
		} else if currentMode == "message" {
			sidebarText += "\n<d>     Delete message\n<b>     Scroll to body\n"
		} else if currentMode == "paniclog" {
			sidebarText += "\n<f>     show full log\n<s>     Switch between paniclog and mainlog\n<c>     Clear log\n"
		}
		sidebarText += "\nVim navigation (hjklgG)"

		hotkeys := tview.NewTextView().
			SetText(sidebarText)
		hotkeys.SetTextColor(globalMenuColor)
		hotkeys.SetBorderPadding(1, 1, 1, 1)
		hotkeys.SetWordWrap(true)
		grid.AddItem(padframe, row, col, 1, 1, mgh, 10, true)
		grid.AddItem(hotkeys, row, col+1, 1, 1, mgh, 10, false)
		// grid.AddItem(hotkeys, row, col+1, 1, 1, mgh, mgw, false)
	}

	grid.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		// infoLogger.Printf("Grid passed on event %c", event.Rune())
		return event
	})

	return grid
}

// Generate the page to hold list view
func generateListPage(sshc *ssh.Client, refresh bool, focus tview.Primitive) {
	title := " Message list "
	if refresh {
		msglist.Clear()
	}
	msglist.ShowSecondaryText(false)
	msglist.SetBorder(true)
	msglist.SetBorderAttributes(tcell.AttrDim)
	msglist.SetBorderPadding(1, 1, 1, 1)
	if currentMta == exim {
		msglist.SetTitle(title + "(mailq) ")
	} else {
		msglist.SetTitle(title + "(postqueue -f) ")
	}

	// Function to load a message
	msglist.SetSelectedFunc(func(i int, main string, secondary string, shortcut rune) {
		if len(main) == 0 || main == "(Empty)" {
			return
		}
		msgid := secondary
		if len(msgid) != 16 && len(msgid) != 11 {
			fmt.Println("Error, selected id <" + msgid + "> seems invalid.")
			return
		}
		currentMode = "message"
		generateMessagePage(sshc, msgid)
	})

	if len(strings.TrimSpace(recipientfilter)) > 0 {
		msglist.SetTitle(msglist.GetTitle() + "filtered on recipient <" + recipientfilter + ">")
	}
	if len(strings.TrimSpace(fromfilter)) > 0 {
		msglist.SetTitle(msglist.GetTitle() + "filtered on sender <" + fromfilter + ">")
	}

	if refresh {
		fromfilterinput = tview.NewInputField()
		recipientfilterinput = tview.NewInputField()

		queueList := fetchMailList(refresh)

		for i := 0; i < len(queueList); i++ {
			msgid, formatted, _ := strings.Cut(queueList[i], ",")
			if len(formatted) < 1 {
				continue
			}
			msglist = msglist.AddItem(formatted, msgid, rune(0), nil)
		}
	}

	pages.AddAndSwitchToPage(
		currentMode,
		generateHeader(msglist),
		true,
	)

	app.SetFocus(focus)
}

// Generate the page to hold paniclog
func generateLogPage(sshc *ssh.Client) {
	list := tview.NewList().ShowSecondaryText(false)
	list.SetBorder(true)
	list.SetBorderPadding(1, 1, 1, 1)
	title := ""
	if showFullLog {
		title += " Logfile " + showLogfile + " "
	} else {
		title += " Last lines of logfile " + showLogfile + " "
	}
	list.SetTitle(title)

	if !fileExists(sshc, showLogfile) {
		list = list.AddItem("(Logfile "+showLogfile+" is missing)", "", rune(0), nil)

	} else {
		logcontent := ""
		if showFullLog {
			logcontent, _ = runCommand(sshc, "$(which cat) "+showLogfile)
		} else {
			logcontent, _ = runCommand(sshc, "$(which tail) "+showLogfile)
		}

		if len(logcontent) == 0 {
			list = list.AddItem("(Logfile "+showLogfile+" is empty)", "", rune(0), nil)
		} else {
			for _, line := range strings.Split(logcontent, "\n") {
				if len(strings.TrimSpace(line)) < 1 {
					continue
				}
				list = list.AddItem(line, "", rune(0), nil)
			}
		}
	}

	pages.AddAndSwitchToPage(
		currentMode,
		generateHeader(list),
		true,
	)
}

// Generate the page to hold sum table
func generateSumPage(sshc *ssh.Client) {
	currentMode = "sum"
	mailqueue := ""
	// cellMaxwidth := 15
	if currentMta == exim {
		mailqueue = fetchMailSumExim()
	} else {
		mailqueue = fetchMailSumPostfix("")
	}

	// Configure table
	sumtable.Clear()
	sumtable.SetBorders(false)
	if *verbose {
		sumtable.SetBorders(true)
	}
	sumtable.SetSelectable(true, false)

	mailList := strings.Split(mailqueue, "\n")
	for row, line := range mailList {
		formatted := strings.TrimSpace(line)
		if len(formatted) < 1 || formatted[:5] == "-----" { // Skip empty lines
			continue
		}

		if row == 0 { // Header row
			for column, field := range strings.Fields(formatted) {
				tc := tview.NewTableCell(field)
				tc.SetAlign(tview.AlignLeft).SetTextColor(tcell.ColorOrange)
				// tc.SetMaxWidth(cellMaxwidth)
				sumtable.SetCell(row, column, tc)
			}
			continue
		}

		// Data row
		for column, field := range strings.Fields(formatted) {
			tc := tview.NewTableCell(field)
			tc.SetAlign(tview.AlignLeft)
			// tc.SetMaxWidth(cellMaxwidth)
			sumtable.SetCell(row, column, tc)
		}
	}

	sumtable.Select(1, 0).SetFixed(1, 0).SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEnter {
			sumtable.SetSelectable(true, true)
		}
	}).SetSelectedFunc(func(row int, column int) {
		// Abort selection if on top row, if row starts with ( or if on last row when exim
		if row == 0 ||
			string(sumtable.GetCell(row, 0).Text)[0:1] == "(" ||
			(currentMta == exim && row == (len(mailList)-1)) {
			return
		}

		recipientfilter = sumtable.GetCell(row, 4).Text
		fromfilter = ""

		currentMode = "list"
		generateListPage(sshc, true, msglist)
	})

	padframe := tview.NewFrame(sumtable)
	padframe.SetBorder(true)
	if currentMta == exim {
		padframe.SetTitle(" Overview (exim -bp|exiqsumm -c) ")
	} else {
		padframe.SetTitle(" Overview (postqueue -f) ")
	}

	pages.AddAndSwitchToPage(
		currentMode,
		generateHeader(padframe),
		true,
	)
}

// Generate the page to hold a single message
func generateMessagePage(sshc *ssh.Client, msgid string) {

	messageContent := loadMessage(sshc, msgid)

	messageviewer = tview.NewTextView()
	messageviewer.SetDynamicColors(true)
	messageviewer.SetRegions(true)
	header := ""
	body := ""

	lines := strings.Split(messageContent, "\r\n")
	for lineNumber, line := range lines {
		header += line + "\n"
		if (currentMta == exim && line == "") ||
			(currentMta == postfix && line == "regular_text: ") {
			infoLogger.Println("Found header-body split at line " + strconv.Itoa(lineNumber))
			messageBodyLine = lineNumber
			body = strings.Join(lines[lineNumber:], "\n")
			break
		}
	}

	messageviewer.SetText("[orange]" + header + "[body][white]" + body)

	pages.AddAndSwitchToPage(
		currentMode,
		generateHeader(messageviewer),
		true,
	)
}

// Check if focus is on a modal (buttons) or a list, table, etc.
func isInAModal() bool {
	primitive := app.GetFocus()
	if _, ok := primitive.(*tview.Button); ok {
		return true
	}
	return false
}

func refreshUI() {
	if currentMode == "sum" {
		generateSumPage(sshc)
	} else if currentMode == "list" {
		generateListPage(sshc, false, msglist)
	} else if currentMode == "paniclog" {
		generateLogPage(sshc)
	} else if currentMode == "message" {
		generateMessagePage(sshc, msgid)
	}
}
