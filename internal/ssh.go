package internal

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"syscall"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"golang.org/x/term"

	"github.com/kevinburke/ssh_config"
	"github.com/mitchellh/go-homedir"
)

func credentials(message string) (string, error) {
	// Get password from user

	fmt.Print(message)
	bytePassword, err := term.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return "", err
	}

	password := string(bytePassword)
	return strings.TrimSpace(password), nil
}

func getSSHConfig(hostName string) string {
	// Get relevant ssh config options from user config

	//https://pkg.go.dev/github.com/kevinburke/ssh_config
	file, _ := homedir.Expand(ssh_config.Get(hostName, "IdentityFile"))
	_, err := os.Open(file)
	if *verbose {
		infoLogger.Println("Config says to use IdentityFile", file)
	}

	if err != nil {
		// fmt.Println(err)
		file, _ = homedir.Expand("~/.ssh/id_rsa")
		if *verbose {
			infoLogger.Println("No config, falling back to file", file)
		}
	}
	fmt.Println("Using IdentityFile <" + file + ">. Override in ~/.ssh/config")

	return file
}

func readPubKey(file string) ssh.AuthMethod {
	// https://snippets.aktagon.com/snippets/846-how-to-ssh-with-go

	var key ssh.Signer
	var b []byte
	b, err := ioutil.ReadFile(file)

	if err != nil {
		fmt.Println("Error accessing keyfile", file, err)
		os.Exit(1)
	}

	if strings.Contains(string(b), "ENCRYPTED") { // encrypted rsa key
		if *verbose {
			infoLogger.Println("readPubKey: found encrypted rsa", file)
		}

		keyPass, _ := credentials("Password for SSH key: ")
		key, err = ssh.ParsePrivateKeyWithPassphrase(b, []byte(keyPass))
		if err != nil {
			fmt.Println("Error parsing keyfile", file)
			os.Exit(1)
		}
		return ssh.PublicKeys(key)
	}

	if strings.Contains(string(b), "OPENSSH PRIVATE KEY") { // encrypted or unencrypted ed25519?
		if *verbose {
			infoLogger.Println("readPubKey: found ed25519", file)
		}
		key, err = ssh.ParsePrivateKey(b)
		if err != nil {
			fmt.Println("Error parsing keyfile. Note about ed25519 type, only unencrypted keys are supported. Using ssh agent will circumvent this.\n", err)
			os.Exit(1)
		}
		return ssh.PublicKeys(key)
	}

	// Unencrypted rsa key
	key, err = ssh.ParsePrivateKey(b)
	if *verbose {
		infoLogger.Println("readPubKey: found unencrypted rsa", file)
	}
	if err != nil {
		fmt.Println("Error parsing keyfile", file)
		os.Exit(1)
	} else {
		return ssh.PublicKeys(key)
	}

	return nil
}

// Attempt ssh agent
func connectAgent(config *ssh.ClientConfig, userName string) *ssh.ClientConfig {

	//Check if running via snap
	snapEnv := os.Getenv("SNAP_USER_COMMON")
	if len(snapEnv) > 0 {
		fmt.Println("Running as a snap, not trying ssh agent")
		return config
	}

	infoLogger.Println("Attempting to login via ssh agent.")
	socket := os.Getenv("SSH_AUTH_SOCK")
	if socket == "" {
		fmt.Println("Warning: Failed to find ssh agent (check --help for possible solutions).")
		return config
	}

	conn, err := net.Dial("unix", socket)
	agentClient := agent.NewClient(conn)
	if err != nil {
		fmt.Println("Warning: Failed to connect to ssh agent (check --help for possible solutions).")
		return config
	}

	config = &ssh.ClientConfig{
		User: userName,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeysCallback(agentClient.Signers),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
	return config
}

func connectKey(config *ssh.ClientConfig, hostName string, userName string) *ssh.ClientConfig {
	// Attempt key
	fmt.Println("Attempting to login via ssh key.")

	// Get relevant config from .ssh/config
	keyfile := getSSHConfig(hostName)
	_, err := os.Stat(keyfile)
	if err == nil {
		config = &ssh.ClientConfig{
			User: userName,
			Auth: []ssh.AuthMethod{
				readPubKey(keyfile),
			},
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		}
	} else {
		infoLogger.Println("Warning: Unable to find an ssh key file (check --help for possible solutions).")
	}
	return config
}

func connectSSH(userName string, hostName string) *ssh.Client {
	if hostName == "localhost" {
		return nil
	}
	var config *ssh.ClientConfig

	config = connectAgent(config, userName)

	if config == nil {
		config = connectKey(config, hostName, userName)
	}

	if config == nil {
		fmt.Println("Attempting to login via ssh password.")
		password, _ := credentials("Password for SSH user " + userName + ": ")
		config = &ssh.ClientConfig{
			User: userName,
			Auth: []ssh.AuthMethod{
				ssh.Password(password),
			},
			HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		}
	}
	if config == nil {
		fmt.Println("Error: No valid ssh auth.")
		os.Exit(1)
	}

	// connect to ssh server
	fmt.Println("Connecting to " + hostName + "...")
	sshc, err := ssh.Dial("tcp", hostName+":22", config)
	if err != nil {
		log.Fatal(err)
	}
	if *verbose {
		infoLogger.Println("Connected to", hostName)
	}

	return sshc
}

// Run command, via ssh or locally
func runCommand(sshc *ssh.Client, command string) (string, int) {
	exitStatus := 0

	if targetHost == "localhost" {
		cmd := exec.Command("bash", "-c", command)
		stdout, err := cmd.Output()
		if err != nil {
			fmt.Println("Local command <" + command + "> gave error " + err.Error())
			exitStatus = 1
		}

		return string(stdout), exitStatus
	}

	session, err := sshc.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	// configure terminal mode
	modes := ssh.TerminalModes{
		ssh.ECHO: 0, // supress echo
	}
	// run terminal session
	if err := session.RequestPty("xterm", 80, 23, modes); err != nil {
		log.Fatal(err)
	}
	// if *verbose {
	// 	InfoLogger.Println("PTY created")
	// }

	var buff bytes.Buffer
	session.Stdout = &buff
	if *verbose {
		infoLogger.Println("RunSSH <" + command + ">")
	}
	err = session.Run(command)

	if err != nil {
		if err.Error() == "Process exited with status 127" {
			fmt.Println("Error, command " + command + " doesn't exist.")
			os.Exit(1)
		} else {
			// if *verbose {
			// 	InfoLogger.Println(session.Stderr)
			// }
			errorFields := strings.Fields(err.Error())
			status := errorFields[len(errorFields)-1]
			exitStatus, _ = strconv.Atoi(status)
			return err.Error(), exitStatus
		}
	}

	return strings.TrimSpace(buff.String()), exitStatus
}

func openTerminal(sshc *ssh.Client, userName string, hostname string) {
	infoLogger.Println("Opening terminal on " + hostname)

	cmd := exec.Command("/usr/bin/x-terminal-emulator", "-e", "ssh", "root@"+hostname)
	stdout, err := cmd.Output()

	if *verbose {
		infoLogger.Println("Output from x-terminal-emulator: " + string(stdout))
	}

	if err != nil {
		fmt.Println(err.Error())
		return
	}
}
