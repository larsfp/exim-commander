package internal

import (
	"encoding/json"
	"strconv"
	"strings"
	"time"

	"github.com/mergestat/timediff"
)

// Fetch an overview of mail in queue, grouped by domain, as string with newlines
// Count  Volume  Oldest  Newest  Domain
func fetchMailSumPostfix(rawjson string) string {
	command := "$(which postqueue) -j"
	if len(rawjson) == 0 { // Allow testing function by supplying json
		rawjson, _ = runCommand(sshc, command)
	}
	if len(strings.TrimSpace(rawjson)) == 0 { // But give up if we got no data
		return ""
	}

	separator := "\n"
	queueLength := strings.Count(rawjson, separator)
	// domainList := make([]DomainSum, queueLength+1)

	type domainSummary struct {
		Domain         string
		Count, Volume  int
		Newest, Oldest int64
	}
	queue := make([]domainSummary, queueLength+1)

	// Iterate mails, collecting domains
	for _, line := range strings.Split(rawjson, "\n") {
		if len(strings.TrimSpace(line)) == 0 {
			break
		}
		// Parse json
		var postqueueEntry PostqueueEntry
		json.Unmarshal([]byte(line), &postqueueEntry)

		// Collect domain from recipients, only caring about first
		recipients := ""
		for _, r := range postqueueEntry.Recipients {
			domain := strings.Split(r.Address, "@")[1]
			recipients += domain
			break
		}
		// arrivalTime := time.Unix(postqueueEntry.ArrivalTime, 0)
		arrivalTime := postqueueEntry.ArrivalTime
		size := postqueueEntry.MessageSize

		// Check if we already have this domain
		domainIndex := -1
		for i, q := range queue {
			if q.Domain == recipients {
				domainIndex = i
				break
			}
		}

		count := 1
		if domainIndex != -1 { // Have it
			queue[domainIndex].Count = queue[domainIndex].Count + count
			queue[domainIndex].Volume = queue[domainIndex].Volume + size
			if queue[domainIndex].Newest < arrivalTime {
				queue[domainIndex].Newest = arrivalTime
			}
			if queue[domainIndex].Oldest > arrivalTime {
				queue[domainIndex].Oldest = arrivalTime
			}
		} else { // Add it
			queue = append(queue, domainSummary{
				Domain: recipients,
				Count:  count,
				Volume: size,
				Newest: arrivalTime,
				Oldest: arrivalTime,
			},
			)
		}
	}

	output := "Count\tVolume\tOldest\tNewest\tDomain\n"
	for _, q := range queue {
		if len(q.Domain) == 0 {
			continue
		}

		output += strconv.Itoa(q.Count) + "\t" +
			strconv.Itoa(q.Volume) + "\t" +
			strings.Replace(timediff.TimeDiff(time.Unix(q.Oldest, 0)), " ", "_", -1) + "\t" +
			strings.Replace(timediff.TimeDiff(time.Unix(q.Newest, 0)), " ", "_", -1) + "\t" +
			q.Domain +
			"\n"
	}
	return output
}
