package internal

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

// Recipients is for converting json from postfix
type Recipients struct {
	Address     string `json:"address"`
	DelayReason string `json:"delay_reason"`
}

// PostqueueEntry is for converting json from postfix
type PostqueueEntry struct {
	QueueName   string       `json:"queue_name"`
	QueueID     string       `json:"queue_id"`
	ArrivalTime int64        `json:"arrival_time"`
	MessageSize int          `json:"message_size"`
	Sender      string       `json:"sender"`
	Recipients  []Recipients `json:"recipients"`
}

// Validate hostname
func isValidHost(host string) bool {
	host = strings.TrimSpace(host)
	if len(host) == 0 {
		return false
	}

	re, _ := regexp.Compile(`^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$`)
	return re.MatchString(host)
}

func parseHost(argv []string) {
	userName = "root"
	args := flag.Args()
	if len(args) == 1 {
		targetHost = args[0]
		if *verbose {
			infoLogger.Println("Target host set to ", targetHost)
		}
	} else if len(args) == 0 {
		return
	} else {
		fmt.Println("Error: bad host, <" + strings.Join(args, " ") + ">")
		os.Exit(1)
	}

	indexOfAt := strings.Index(args[0], "@")
	if indexOfAt > 0 {
		// User given, parse
		userName = args[0][0:indexOfAt]
		targetHost = args[0][indexOfAt+1:]

		if *verbose {
			infoLogger.Println("Username <" + userName + ">, hostname <" + targetHost + ">")
		}
	}

	if !isValidHost(targetHost) {
		fmt.Println("Error: host " + args[0] + " is invalid.")
		os.Exit(1)
	}
}

func exit(sshc *ssh.Client, backgroundStop chan bool) {
	//backgroundTicker.Stop()
	backgroundStop <- true
	if sshc != nil {
		sshc.Close()
	}
	app.Stop()
}

func selectMTA(eximStatus int, postfixStatus int) bool {
	if eximStatus == 0 {
		currentMta = exim
		if *verbose {
			infoLogger.Println("Found exim")
		}
		return true
	} else if postfixStatus == 0 {
		currentMta = postfix
		if *verbose {
			infoLogger.Println("Found postfix")
		}
		return true
	}
	return false
}

// Fetch list of mails. Refresh list from server only if demanded.
// Returned is a list of strings on the format message_id,message_line
func fetchMailList(refresh bool) []string {
	queuecontent := ""
	command := ""

	// If MTA is exim
	if currentMta == exim {
		command = "$(which exiqgrep)"
		if len(strings.TrimSpace(recipientfilter)) > 0 {
			command += " -r " + strings.TrimSpace(recipientfilter)
		}
		if len(strings.TrimSpace(fromfilter)) > 0 {
			command += " -f " + strings.TrimSpace(fromfilter)
		}
		queuecontent, _ = runCommand(sshc, command)

		if len(queuecontent) == 0 {
			output := [1]string{",(Empty)"}
			return output[:]
		}

		separator := "\r\n\r\n"
		queueLength := strings.Count(queuecontent, separator)
		output := make([]string, queueLength+1)

		for lineNumber, line := range strings.Split(queuecontent, separator) {
			formatted := strings.TrimSpace(line)
			if len(formatted) < 1 {
				continue
			}

			msgid := strings.Fields(formatted)[2]
			if len(msgid) != 16 {
				fmt.Println("Error, selected id <" + msgid + "> seems invalid.")
				continue
			}

			output[lineNumber] = msgid + "," + formatted
			// msglist = msglist.AddItem(formatted, msgid, rune(0), nil)
		}
		return output
	}

	// If MTA is Postfix
	command = "$(which postqueue) -j"
	rawjson, _ := runCommand(sshc, command)

	separator := "\r\n"
	queueLength := strings.Count(rawjson, separator)
	output := make([]string, queueLength+1)

	for lineNumber, line := range strings.Split(rawjson, "\r\n") {
		if len(line) == 0 {
			break
		}
		var postqueueEntry PostqueueEntry
		json.Unmarshal([]byte(line), &postqueueEntry)

		recipients := ""
		for _, r := range postqueueEntry.Recipients {
			recipients += r.Address + "(" + r.DelayReason[:10] + "), "
		}
		if len(strings.TrimSpace(recipientfilter)) > 0 && !strings.Contains(recipients, recipientfilter) {
			continue
		}

		arrivalTime := time.Unix(postqueueEntry.ArrivalTime, 0)

		output[lineNumber] = postqueueEntry.QueueID + "," +
			arrivalTime.String() + " " +
			strconv.Itoa(postqueueEntry.MessageSize) + " " +
			postqueueEntry.QueueID + " <" +
			postqueueEntry.Sender + "> " +
			recipients
	}

	return output
}

// Slice string if long
func returnSlice(input string, lenght int) string {
	if len(input) <= lenght {
		return input
	}
	return input[0:lenght]
}

// Switch between logs shown
func switchLogfile() {
	if currentMta == exim {
		if showLogfile == eximPanicLog {
			showLogfile = eximMainLog
		} else {
			showLogfile = eximPanicLog
		}
	} else { //Postfix
		if showLogfile == postfixErrorLog {
			showLogfile = postfixMainLog
		} else {
			showLogfile = postfixErrorLog
		}
	}
}

//Check if running via snap
func checkPermission() {
	snapEnv := os.Getenv("SNAP_USER_COMMON")
	if len(snapEnv) > 0 {
		if *verbose {
			fmt.Println("Running as a snap.")
		}

		if sshc == nil {
			fmt.Println("Error: you are running a snap, so I have no access to exim, postfix commands. Run via ssh, i.e. excom root@<your local IP>")
			os.Exit(1)
		}
	}

	if targetHost == "localhost" {
		output, _ := runCommand(sshc, "whoami")
		output = strings.TrimSpace(output)
		if output != "root" {
			fmt.Println("Warning: you may not have privileges to run necessary commands on this host. Run as root, not <" + output + ">.")
		}
	}
}

// Return actual content of what's in focus
func getContent() string {
	output := ""

	if currentMode == "sum" {
		// for _, row := range sumtable.GetRowCount() {
		output += "sum content"

	} else if currentMode == "list" {
		output += "list content"

	} else if currentMode == "paniclog" {
		output += "log content"

	} else if currentMode == "message" {
		output += messageviewer.GetText(true)
	}

	return output
}
