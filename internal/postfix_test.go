package internal

import (
	"fmt"
	"strings"
	"testing"
)

func Test_fetchMailListPostfix(t *testing.T) {
	rawjson := " "
	output := fetchMailSumPostfix(rawjson)

	if output != "" {
		t.Fatalf(`fetchMailListPostfix("") should give empty array, but gave %s`, output)
	}

	rawjson = `{"queue_name": "deferred", "queue_id": "A8BE8A82871", "arrival_time": 1661171479, "message_size": 407, "forced_expire": false, "sender": "larsfp@laptop1016.lan", "recipients": [{"address": "someone@example.com", "delay_reason": "Host or domain name not found. Name service error for name=none.copyleft.no type=AAAA: Host not found"}]}
	{"queue_name": "deferred", "queue_id": "0104CA815D5", "arrival_time": 1661195645, "message_size": 408, "forced_expire": false, "sender": "larsfp@laptop1016.lan", "recipients": [{"address": "sometwo@example.com", "delay_reason": "Host or domain name not found. Name service error for name=none.copyleft.no type=AAAA: Host not found"}]}
	`

	output = fetchMailSumPostfix(rawjson)

	if !(strings.Contains(output, "example.com")) {
		t.Fatalf(`fetchMailListPostfix(<some json>) should contain example.com, but gave %s`, output[:11])
	}

	// Only shown when testing with -v
	fmt.Println("Full string: <" + output + ">")
}
