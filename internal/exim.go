package internal

import (
	"fmt"
	"strconv"
	"strings"

	"golang.org/x/crypto/ssh"
)

func fileExists(sshc *ssh.Client, filename string) bool {
	_, exitStatus := runCommand(sshc, "test -f "+filename)
	if exitStatus != 0 {
		if *verbose {
			infoLogger.Println("fileExists " + filename + " false")
		}
		return false
	}
	if *verbose {
		infoLogger.Println("fileExists " + filename + " true")
	}
	return true
}

func getPaniclogLength(sshc *ssh.Client) int {
	paniclog, _ := runCommand(sshc, "$(which wc) --lines "+panicLogFile)
	paniclogLength, _ := strconv.Atoi(strings.Fields(paniclog)[0])

	if *verbose {
		infoLogger.Println("PaniclogLength " + panicLogFile + ": " + strconv.Itoa(paniclogLength))
	}
	return paniclogLength
}

func loadMessage(sshc *ssh.Client, msgid string) string {
	if currentMta == exim {
		messageContent, _ := runCommand(sshc, "$(which exim) -Mvc "+msgid)
		return messageContent
	}
	messageContent, _ := runCommand(sshc, "$(which postcat) -vq "+msgid)
	return messageContent
}

// Delete all messages with "domain" matching recipient
func deleteMessageByDomain(sshc *ssh.Client, domain string) {
	cmd := "/usr/sbin/exiqgrep -i -r @" + domain + " | /usr/bin/xargs exim -Mrm"
	if currentMta == postfix {
		domain = strings.Replace(domain, ".", `\.`, -1)
		cmd = `$(which postqueue) -p | tail -n +2 | awk 'BEGIN { RS = "" } /@` + domain + `/ { print $1 }' | tr -d '*!' | $(which postsuper) -d -`
	}

	if len(domain) == 0 {
		fmt.Println("Error, no domain given to DeleteMessageByDomain")
		return
	}

	if *verbose {
		infoLogger.Println("Deleting all messages matching <" + domain + ">.")
	}

	result, _ := runCommand(sshc, cmd)
	if *verbose {
		infoLogger.Println("Result: <" + result + ">.")
	}
}

// Delete message by id
func deleteMessageByID(sshc *ssh.Client, msgid string) {
	cmd := "$(which exim) -Mrm " + msgid
	if currentMta == postfix {
		cmd = "$(which postsuper) -d " + msgid
	}
	if len(msgid) == 0 {
		infoLogger.Println("Error, no msgid given to deleteMessageByID")
		return
	}

	if *verbose {
		infoLogger.Println("Deleting message <" + msgid + ">.")
	}

	result, _ := runCommand(sshc, cmd)
	if *verbose {
		infoLogger.Println("Result: <" + result + ">.")
	}
}

func truncateFile(sshc *ssh.Client, filepath string) bool {
	if len(filepath) < 4 || filepath[0:1] != "/" {
		infoLogger.Println("Error, sanity check for truncateFile <" + filepath + "> failed.")
		return false
	}

	_, exitStatus := runCommand(sshc, "truncate --size=0 "+filepath)
	if exitStatus != 0 {
		if *verbose {
			infoLogger.Println("truncateFile <" + filepath + "> failed.")
		}
		return false
	}
	if *verbose {
		infoLogger.Println("truncateFile <" + filepath + "> success.")
	}
	return true
}

// Fetch an overview of mail in queue, usually sorted by domain, as string with newlines
func fetchMailSumExim() string {
	mailqueue, _ := runCommand(sshc, "$(which exim) -bp | $(which exiqsumm) -c")
	return mailqueue
}
