package internal

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"os"

	"github.com/atotto/clipboard"
	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"
	"golang.org/x/crypto/ssh"
)

type mtaType int64

const (
	projectName    = "excom"
	projectVersion = "0.7.14"

	hostnameCMD       = "$(which hostname) -f"
	eximversionCMD    = "which exim && $(which exim) --version"
	postfixversionCMD = "which postconf && $(which postconf) mail_version"
	eximMainLog       = "/var/log/exim4/mainlog"
	eximPanicLog      = "/var/log/exim4/paniclog"
	postfixMainLog    = "/var/log/mail.log"
	postfixErrorLog   = "/var/log/mail.err"

	// Enums for MTA
	nomta mtaType = iota
	exim
	postfix
)

var (
	verbose        *bool
	infoLogger     *log.Logger
	sshc           *ssh.Client
	backgroundStop chan bool

	// State
	paniclogLength  int
	fqdn            string
	fromfilter      string
	recipientfilter string
	messageBodyLine int
	currentMta      mtaType
	panicLogFile    string
	msgid           string
	currentMode     string
	showLogfile     string
	showFullLog     bool
	targetHost      string
	userName        string
	showSidebar     bool

	// UI
	app                  *tview.Application
	pages                *tview.Pages
	sumtable             *tview.Table
	msglist              *tview.List
	backgroundTicker     *time.Ticker
	fromfilterinput      *tview.InputField
	recipientfilterinput *tview.InputField
	grid                 *tview.Grid
	messageviewer        *tview.TextView
)

func init() {
	verbose = flag.Bool("v", false, "Show verbose output.")
	flag.StringVar(&targetHost, "t", "localhost", "Hostname to connect to. Default is localhost, which does not use SSH.")
}

// Execute will start the app
func Execute() {
	currentMode = "sum"
	showFullLog = false
	showSidebar = true

	// Parse args, connect to host
	showHelp := flag.Bool("h", false, "Show this help."+
		"\n\nIf installed via snap, some limitations apply:\n"+
		" * you can not run on localhost, you have to work via ssh.\n"+
		" * SSH agent can't be used at all.\n"+
		" * You need to allow access to use ssh-keys: $ sudo snap connect excom:ssh-keys :ssh-keys\n\n"+
		"Project source https://gitlab.com/larsfp/excom\n")
	showVersion := flag.Bool("version", false, "Show version.")

	flag.Parse()
	if *showHelp {
		flag.PrintDefaults()
		os.Exit(0)
	}
	if *showVersion {
		fmt.Println(projectName + " version " + projectVersion)
		checkPermission()
		os.Exit(0)
	}
	infoLogger = &log.Logger{}
	infoLogger = log.New(os.Stderr, "", log.Ldate|log.Ltime|log.Lshortfile)
	if !*verbose {
		infoLogger.SetOutput(ioutil.Discard)
	}

	parseHost(flag.Args())

	sshc = connectSSH(userName, targetHost)

	// Find MTA
	checkPermission()
	eximVersion, eximErr := runCommand(sshc, eximversionCMD)
	postfixVersion, postfixErr := runCommand(sshc, postfixversionCMD)
	if !selectMTA(eximErr, postfixErr) {
		fmt.Println("No known MTA (exim, postfix) found on host", targetHost)
		os.Exit(1)
	}
	if currentMta == exim {
		panicLogFile = eximPanicLog
	} else {
		panicLogFile = postfixErrorLog
	}
	showLogfile = panicLogFile

	if *verbose {
		infoLogger.Println("Connected to " + targetHost + ". Found " + returnSlice(eximVersion, 20) + returnSlice(postfixVersion, 20))
	}

	// Background job to update paniclog length, SumPage, PaniclogPage
	// TODO: refresh list too, but keep position?
	backgroundTicker = time.NewTicker(20 * time.Second)
	backgroundStop = make(chan bool)
	go func() {
		for {
			select {
			case <-backgroundStop:
				return
			case t := <-backgroundTicker.C:
				infoLogger.Println("backgroundTicker at", t)
				paniclogLength = getPaniclogLength(sshc)

				pagetitle, _ := pages.GetFrontPage()
				if pagetitle == "sum" {
					generateSumPage(sshc)
				} else if pagetitle == "paniclog" {
					generateLogPage(sshc)
				}
			}
		}
	}()

	paniclogLength = 0
	if fileExists(sshc, panicLogFile) {
		paniclogLength = getPaniclogLength(sshc)
	} else {
		paniclogLength = 0
	}

	// Get target hostname
	fqdn, _ = runCommand(sshc, hostnameCMD)
	fqdn = strings.TrimSpace(fqdn)
	if *verbose {
		infoLogger.Println("fqdn", fqdn)
	}

	app = tview.NewApplication()
	pages = tview.NewPages()
	sumtable = tview.NewTable()
	msglist = tview.NewList()
	msgid = ""

	// Handle all hotkeys
	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {

		// Handle enter and esc in list-> filters
		if fromfilterinput != nil {
			if fromfilterinput.HasFocus() {
				if event.Key() == tcell.KeyEnter {
					return event
				}
				if event.Key() == tcell.KeyEscape {
					fromfilter = ""
					fromfilterinput.SetText(fromfilter)
					generateListPage(sshc, true, msglist)
				}
				return event
			}
		}
		if recipientfilterinput != nil {
			if recipientfilterinput.HasFocus() {
				if event.Key() == tcell.KeyEnter {
					return event
				}
				if event.Key() == tcell.KeyEscape {
					recipientfilter = ""
					recipientfilterinput.SetText(recipientfilter)
					generateListPage(sshc, true, msglist)
				}
				return event
			}
		}

		if event.Key() == tcell.KeyEscape {
			if currentMode == "message" {
				currentMode = "list"
				generateListPage(sshc, true, msglist)
			} else if currentMode != "sum" {
				generateSumPage(sshc)
			}

		} else if event.Rune() == 'q' {
			infoLogger.Println("Exiting normally.")
			exit(sshc, backgroundStop)
			return nil

		} else if event.Rune() == '1' {
			generateSumPage(sshc)

		} else if event.Rune() == '3' {
			currentMode = "paniclog"
			generateLogPage(sshc)

		} else if event.Rune() == '2' {
			currentMode = "list"
			generateListPage(sshc, true, msglist)

		} else if event.Rune() == 't' {
			openTerminal(sshc, userName, fqdn)

		} else if event.Rune() == 'c' {
			if currentMode == "paniclog" {
				if paniclogLength == 0 {
					return nil
				}

				modal := tview.NewModal().
					SetText("Delete log " + showLogfile + "?").
					AddButtons([]string{"Delete", "Cancel"}).
					SetDoneFunc(func(buttonIndex int, buttonLabel string) {
						if buttonLabel == "Delete" {
							truncateFile(sshc, showLogfile)

						}
						generateLogPage(sshc)
						app.SetRoot(pages, true)
					})
				app.SetRoot(modal, false).SetFocus(modal)
			}

		} else if event.Rune() == 'f' {
			// in list, filter based on sender
			if currentMode == "list" {

				if len(fromfilter) == 0 {
					fromfilter = " "
				}
				generateListPage(sshc, false, fromfilterinput)
				return nil
			}
			if currentMode == "paniclog" {
				showFullLog = !showFullLog
				generateLogPage(sshc)
				return nil
			}

		} else if event.Rune() == 'r' {
			// in list, filter based on recipient
			if currentMode == "list" {

				if len(recipientfilter) == 0 {
					recipientfilter = " "
				}
				generateListPage(sshc, false, recipientfilterinput)
				return nil
			}

		} else if event.Rune() == 's' {
			if currentMode == "paniclog" {
				switchLogfile()
				generateLogPage(sshc)
			}
			return nil

		} else if event.Rune() == 'b' {
			showSidebar = !showSidebar
			refreshUI()
			return nil

		} else if event.Rune() == 'y' {
			clipboard.WriteAll(getContent())
			return nil

		} else if event.Rune() == 'd' {
			// In "sum", confirm if messages should be deleted
			if currentMode == "sum" {
				row, _ := sumtable.GetSelection()
				if row == 0 {
					return nil
				}
				domain := sumtable.GetCell(row, 4).Text
				// fmt.Println(sumtable.GetCell(row, 0).Text)
				if len(domain) == 0 || domain == "TOTAL" {
					return nil
				}

				modal := tview.NewModal().
					SetText("Delete all messages with recipient matching @" + domain).
					AddButtons([]string{"Delete", "Cancel"}).
					SetDoneFunc(func(buttonIndex int, buttonLabel string) {
						if buttonLabel == "Delete" {
							deleteMessageByDomain(sshc, domain)
							generateSumPage(sshc)
						}
						app.SetRoot(pages, true)
						generateSumPage(sshc)
					})
				app.SetRoot(modal, false).SetFocus(modal)

			} else if currentMode == "list" {
				message := ""
				message, msgid = msglist.GetItemText(msglist.GetCurrentItem())
				if len(msgid) == 0 {
					return nil
				}

				modal := tview.NewModal().
					SetText("Delete message with id <" + msgid + ">?\n\n<" + message + ">").
					AddButtons([]string{"Delete", "Cancel"}).
					SetDoneFunc(func(buttonIndex int, buttonLabel string) {
						if buttonLabel == "Delete" {
							deleteMessageByID(sshc, msgid)
						}
						generateListPage(sshc, true, msglist)
						app.SetRoot(pages, true)
					})
				app.SetRoot(modal, false).SetFocus(modal)

			} else if currentMode == "message" {
				message := ""
				message, msgid = msglist.GetItemText(msglist.GetCurrentItem())
				if len(msgid) == 0 {
					return nil
				}

				modal := tview.NewModal().
					SetText("Delete message with id <" + msgid + ">?\n\n<" + message + ">").
					AddButtons([]string{"Delete", "Cancel"}).
					SetDoneFunc(func(buttonIndex int, buttonLabel string) {
						if buttonLabel == "Delete" {
							deleteMessageByID(sshc, msgid)
							generateListPage(sshc, true, msglist)
						}
						app.SetRoot(pages, true)
						generateListPage(sshc, true, msglist)
					})
				app.SetRoot(modal, false).SetFocus(modal)

			} else {
				fmt.Println("Not implemented.")
			}

		} else if event.Rune() == 'b' {
			if currentMode != "message" {
				return event
			}

			messageviewer.ScrollTo(messageBodyLine, 0)

			// Vim nav in list
		} else if event.Rune() == 'j' {
			if currentMode == "list" {
				return tcell.NewEventKey(tcell.KeyDown, rune(tcell.KeyDown), tcell.ModNone)
			}
		} else if event.Rune() == 'k' {
			if currentMode == "list" {
				return tcell.NewEventKey(tcell.KeyUp, rune(tcell.KeyUp), tcell.ModNone)
			}
		} else if event.Rune() == 'g' {
			if currentMode == "list" {
				return tcell.NewEventKey(tcell.KeyHome, rune(tcell.KeyHome), tcell.ModNone)
			}
		} else if event.Rune() == 'G' {
			if currentMode == "list" {
				return tcell.NewEventKey(tcell.KeyEnd, rune(tcell.KeyEnd), tcell.ModNone)
			}

		} else if event.Key() == tcell.KeyRight {
			if isInAModal() {
				return event
			}

			// Else, switch page
			if currentMode == "sum" {
				currentMode = "list"
				generateListPage(sshc, true, msglist)
			} else if currentMode == "list" {
				currentMode = "paniclog"
				generateLogPage(sshc)
			} else if currentMode == "paniclog" {
				generateSumPage(sshc)
			}
			//  else {
			// 	fmt.Println("Switch to " + currentMode + "?")
			// }

		} else if event.Key() == tcell.KeyLeft {
			if isInAModal() {
				return event
			}

			if currentMode == "list" {
				generateSumPage(sshc)
			} else if currentMode == "paniclog" || currentMode == "message" {
				currentMode = "list"
				generateListPage(sshc, true, msglist)
			} else if currentMode == "sum" {
				currentMode = "paniclog"
				generateLogPage(sshc)
			}
		}

		// infoLogger.Printf("App passed on event %c, focus at %c", event.Rune(), app.GetFocus())
		return event
	})

	generateSumPage(sshc)

	if err := app.SetRoot(pages, true).EnableMouse(false).Run(); err != nil {
		panic(err)
	}
}
