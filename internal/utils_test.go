package internal

import "testing"

func Test_isValidHost(t *testing.T) {

	if !isValidHost("example.com") {
		t.Fatalf(`isValidHost("example.com") should be valid, but gave false.`)
	}

	if isValidHost("") {
		t.Fatalf(`isValidHost("") should NOT be valid, but gave true.`)
	}
}
